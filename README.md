# PairTracker

## General

PairTracker is meant to be used with the SteamVR plugin for Unity.
Its purpose is to associate a specific tracker to a GameObject when several trackers are connected to the system.
PairTracker is used to disambiguate which tracker is associated with the GameObject.

## How to use

Add a `PairTracker` and a `SteamVR_TrackedObject` component to the GameObject you want to track.
In the inspector, set the Tracker ID to the reference of the physical tracker you want to associate.
This value can be found in the SteamVR app and ressembles this: `LHR-EC913C2D`.
`PairTracker` will associate the tracker to the `SteamVR_TrackedObject` in the `Start` routine.
