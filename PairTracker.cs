using UnityEngine;
using Valve.VR;
using System.Text;

public class PairTracker : MonoBehaviour
{
    public string trackerID;

    // Start is called before the first frame update
    void Start()
    {
        if (OpenVR.System == null)
        {
            Debug.LogError("PairTracker: Could not find VR system");
            return;
        }

        SteamVR_TrackedObject tracked = GetComponent<SteamVR_TrackedObject>();
        bool found = false;
        for (uint i = 0; i < SteamVR.connected.Length; ++i)
        {
            ETrackedPropertyError error = new ETrackedPropertyError();
            StringBuilder sb = new StringBuilder();
            OpenVR.System.GetStringTrackedDeviceProperty(i, ETrackedDeviceProperty.Prop_SerialNumber_String, sb, OpenVR.k_unMaxPropertyStringSize, ref error);
            string serial = sb.ToString();
            if (serial == trackerID)
            {
                tracked.index = (SteamVR_TrackedObject.EIndex)i;
                found = true;
                break;
            }
        }
        if (!found) Debug.LogError("PairTracker: Could not find tracker");
    }
}
